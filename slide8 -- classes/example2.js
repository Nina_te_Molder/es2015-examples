import React from 'react';
import ReactDOM from 'react-dom';

import Article from './react/article';

ReactDOM.render(
  <Article />,
  document.getElementById("content")
);
