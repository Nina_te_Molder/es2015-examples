import Component from './component';

class Header extends Component {

  constructor (id, content) {
    super("h1", id, content);
  }

}

export default Header;
