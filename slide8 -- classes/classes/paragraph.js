import Component from './component';

class Paragraph extends Component {

  constructor (id, content) {
    super("p", id, content);
  }

}

export default Paragraph;
