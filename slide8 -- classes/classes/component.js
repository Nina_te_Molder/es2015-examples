class Component {

  constructor (tagName, id, content) {
    this.tagName = tagName;
    this.id = id;
    this.content = content;
  }

  render () {
    let element = document.getElementById(this.id);
    element.innerHTML += `<${this.tagName}>${this.content}</${this.tagName}>`;
  }

}

export default Component;
