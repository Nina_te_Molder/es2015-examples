import expect from 'expect';

import React from "react";
import ShallowTestUtils from 'react-shallow-testutils';
import TestUtils from 'react-addons-test-utils';

import Header from './react/header';

describe('components', () => {

  let props;
  let output;
  let renderer;

  beforeEach((done) => {
    props = {
      content: "Header Component"
    };

    renderer = TestUtils.createRenderer();
    renderer.render(<Header {...props} />);
    output = renderer.getRenderOutput();

    done();
  });

  it("should have a header with the content 'Header Component'", () => {
    let header = ShallowTestUtils.findWithType(output, 'h1');
    expect(header.props.children).toBe('Header Component');
  });

});
