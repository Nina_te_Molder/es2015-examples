import React, { Component } from "react";

class Paragraph extends Component {

  render () {
    return (
      <p>{this.props.content}</p>
    );
  }

}

export default Paragraph;
