'use strict';

var webpack = require('webpack'),
  path = require('path');

module.exports = {
  entry: {
    example1: "./example1.js",
    example2: "./example2.js"
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].js"
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: "babel"}
    ]
  }
};
