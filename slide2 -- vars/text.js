Hoisting
-------------------------------------

for (var i = 0; i < 10; i++) {
  i;
}
console.log("The number is:", i);

------------------V------------------

var i;
for (i = 0; i < 10; i++) {
  i;
}
console.log("The number is:", i);

// Een let variable is gescoped binnen het block waar hij wordt gedefinieerd
// en wordt NIET gehoist.
