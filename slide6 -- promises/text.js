>> http://pouchdb.com/2015/05/18/we-have-a-problem-with-promises.html?utm_source=javascriptweekly&utm_medium=email
>> https://www.youtube.com/watch?v=hf1T_AONQJU

If you read the literature on promises, you'll often find references to the pyramid of doom,
with some horrible callback-y code that steadily stretches toward the right side of the screen.

Promises do indeed solve this problem, but it's about more than just indentation.
As explained in the brilliant talk "Redemption from Callback Hell",
the real problem with callbacks it that they deprive us of keywords like return and throw.
Instead, our program's entire flow is based on side effects: one function incidentally calling another one.

And in fact, callbacks do something even more sinister: they deprive us of the stack,
which is something we usually take for granted in programming languages.
Writing code without a stack is a lot like driving a car without a brake pedal:
you don't realize how badly you need it, until you reach for it and it's not there.

The whole point of promises is to give us back the language fundamentals we lost when we went async:
return, throw, and the stack.

A promise can only succeed or fail once.
It cannot succeed or fail twice, neither can it switch from success to failure or vice versa
If a promise has succeeded or failed and you later add a success/failure callback,
the correct callback will be called, even though the event took place earlier

It’s very important to understand how to work with JavaScript’s single-thread model.
Otherwise, we might accidentally freeze the entire app.
Once the browser blocks executing a script, it stops running other scripts, rendering elements,
and responding to user events like keyboard and mouse interactions.

In continuation-passing style (CPS) async programming, we tell a function how to continue
execution by passing callbacks. It can grow to complicated nested code.
You throw away the stack once you go async.

A Promise is a new abstraction that allows us to write async code in an easier way.
