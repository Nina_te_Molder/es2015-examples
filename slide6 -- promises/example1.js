// Promises zijn een beter alternatief voor doorgeven van een callback
// - geen pyramid of doom
// - betere error handling (we kunnen gebruik maken van throw en catch)
// - je kan return gebruiken en bent niet meer afhankelijk van side effects

function request (url, callback) {
  setTimeout(function () {
    callback({status: `Fetching ${url} was a success!`});
  }, 1000);
};

// try {
  request('http://www.example.com', function (data) {
    console.log(data.status);
    request('http://www.other-example.com', function (data2) {
      console.log(data2.status);

      // Er is geen mogelijkheid om een error throwen (en te catchen).
      // throw new Error("Oh no!");

      // Er is geen mogelijkheid om iets te returnen
      // return {data: "foo"};
      // Er is niks om de return waarde te ontvangen

      request('http://www.and-another-example.com', function (data3) {
        console.log(data3.status);
      });
    });
  });
// Catch werkt niet met callbacks
// } catch (e) {
//   console.log("hier!", e);
// }

// Een promise is een asynchrone waarde
// Een object die op een bepaald punt in de toekomst een waarde heeft

function requestPromise (url, data) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      // Van pending >> resolved
      resolve({status: `Fetching ${url} with ${JSON.stringify(data)} was a success!`, data: data});
      // Van pending >> rejected
      // reject(new Error(`Fetching ${url} failed...`));
    }, 1000);
  });
};

function getUser (id) {
  return requestPromise("http://www.example.com/api/users", {"id": id});
}

function getArticles (response) {
  console.log(response.status);
  return requestPromise(`http://www.example.com/api/users/${response.data.id}`, {"articles": ["Artikel 1", "Artikel 2"]})
}

function log (response) {
  console.log(response.status);
  console.log(response.data.articles);
};

getUser(2)
  .then(getArticles)
  .then(log)
  .catch(e => console.log(e));
