// Je kan meerder requests parallel laten uitvoeren,
// then() wordt pas uitgevoerd als alle promises zijn geresolved.

// Er wordt een array van promises doorgegeven aan Promise.all

function request (url) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      console.log(`Finished fetching ${url}...`);
      resolve({status: `Fetching ${url} was a success!`});
    }, Math.random() * 1000);
  });
};

let urls = ['http://www.example.com',
  'http://www.example2.com',
  'http://www.example3.com',
  'http://www.example4.com'];

Promise.all(urls.map(function (url) {
  return request(url);
})).then(function (results) {
  console.log(`${results.length} urls where fetched succesfully`);
}).catch(console.log.bind(console));
