import { multiply, add } from './modules/util-functions';
import * as utils from './modules/util-functions';
import square from './modules/util-functions';

let result1 = multiply(3, 5);
let result1 = utils.multiply(3, 5);
let result2 = add(3, 5);
let result2 = utils.add(3, 5);
let result3 = square(3);

console.log('result1', result1);
console.log('result2', result2);
console.log('result3', result3);
