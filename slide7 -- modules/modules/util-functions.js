export function multiply (x, y) {
  return x * y;
}

export function add (x, y) {
  return x + y;
}

export default function square (x) {
  return x * x;
}
