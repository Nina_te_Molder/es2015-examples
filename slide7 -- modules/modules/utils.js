let Utils = function () {

  function square (x) {
    return x * x;
  }

  return {
    square
  };

}();

export default Utils;
