import expect from "expect";

import { multiply, add } from './modules/util-functions';

describe("utils", () => {
  it("should return 15 when multiplying 3 and 5", () => {
    expect(multiply(3, 5)).toBe(15);
  });

  it("should return 8 when 3 is added to 5", () => {
    expect(add(3, 5)).toBe(8);
  });
});
