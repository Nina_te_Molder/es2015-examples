// Immutable

// Een handige toepassing van Object.assign is als je wilt werken met immutable objecten
// In plaats van een object muteren, kan je een kopie maken van het oude object met de nieuwe waarden
// Het oude object blijft bewaard en de huidige staat is opgeslagen in het nieuwe object

let newArticle;

let oldArticle = {title: 'Artikel 1', updatedAt: new Date(2014, 10, 5)};
newArticle = oldArticle;
// Het probleem met het muteren van objecten is dat je je history kwijt raakt EN
// je kan niet meer zien of er daadwerkelijk iets is aangepast aan de data
newArticle.updatedAt = new Date();
console.log('title', newArticle.title, 'updated at', newArticle.updatedAt);
console.log(oldArticle === newArticle);

let history = [];
history.push(oldArticle);
newArticle = Object.assign({}, oldArticle, {
  updatedAt: new Date
});

console.log('title', newArticle.title, 'updated at', newArticle.updatedAt);
// Je kan nu zien of de data is aangepast ZONDER een deep comparison te doen
console.log(oldArticle === newArticle);

// Ook kan je de objecten in een history opslaan en een undo methode worden geimplemeteerd
history.push(newArticle);
console.log(history[0].updatedAt);

// Je kan ook een library gebruiken met uitgebreidere mogelijkheden
// >> https://facebook.github.io/immutable-js/
// >> https://github.com/rtfeldman/seamless-immutable
