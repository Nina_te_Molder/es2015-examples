// Object.assign

// Mergen van twee objecten in een ander object
// Het options object overruled het default object

let defaults = {
  container: 'best-of-list',
  amount: 5,
  syncTime: 2,
  timeUnit: 'minutes'
};

let options = {
  amount: 3,
  syncTime: 10
};

let settings = Object.assign({}, defaults, options);
console.log(settings);
