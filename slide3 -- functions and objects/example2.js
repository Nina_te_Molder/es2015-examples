// Rest parameters

// The arguments object is not an Array.
// It is similar to an Array, but does not have any Array properties except length.
// >> https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/arguments
function render () {
  console.log("arguments is an array?", arguments instanceof Array);
  for (var title in arguments) {
    console.log(arguments[title]);
  }
  // Dit geeft een error, arguments support niet de map methode
  // arguments.map(function (title) {
  //   console.log(title);
  // });
};

// articles is wel een array met alle array methods zoals bijvoorbeeld map
function renderRest (...articles) {
  console.log("articles is an array?", articles instanceof Array);
  articles.map(function (title) {
    console.log(title);
  });
};

render("title 1", "title 2", "title 3");
renderRest("title 1", "title 2", "title 3");
