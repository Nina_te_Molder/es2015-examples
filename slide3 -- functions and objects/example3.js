// Spread parameters

// Het omgekeerde van Rest parameters
// Waarden uit een array gebruiken als individuele argumenten van een methode

function render (article1, article2, article3) {
  console.log("article 1:", article1);
  console.log("article 2:", article2);
  console.log("article 3:", article3);
};

let articles = ["title 1", "title 2", "title 3"];
// render(articles[0], articles[1], articles[2]);
render(...articles);
