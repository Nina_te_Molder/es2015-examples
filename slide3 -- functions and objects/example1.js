// Arrow function

// Arrow functions zorgen ervoor dat this verwijst naar waar de functie is gedefinieerd
// niet naar waar deze wordt aageroepen.
// Dit wordt ook wel lexical binding genoemd.

function Component(props) {
  this.props = props;
  this.render();
}

Component.prototype.render = function () {
  // let articlesES5 = this.props.articles.map(function (articleName) {
  //   return this.props.brand + " " + articleName;
  // // });
  // }.bind(this));

  let articlesES2015 = this.props.articles.map((articleName) => {
    return this.props.brand + " " + articleName;
  });

  // Shorthand variant zonder haakjes
  // articlesES2015 = this.props.articles.map(articleName => this.props.brand + " " + articleName);
  // Shorthand variant met gebruikt van String Templates
  // >> https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/template_strings
  // articlesES2015 = this.props.articles.map(articleName => `${this.props.brand} ${articleName}`);

  console.log("Articles:", articlesES2015.join(", "));
}

let list = new Component({
  "brand": "Volkskrant",
  "articles": [
    "title 1",
    "title 2",
    "title 3"
  ]
})
