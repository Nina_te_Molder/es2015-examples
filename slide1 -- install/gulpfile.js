var gulp = require("gulp");
var babel = require("gulp-babel");

gulp.task("default", function () {
  return gulp.src("example1.js")
    .pipe(babel())
    .pipe(gulp.dest("dist-gulp"));
});
