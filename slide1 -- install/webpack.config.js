'use strict';

var webpack = require('webpack'),
  path = require('path');

module.exports = {
  entry: "./example1.js",
  output: {
      path: path.join(__dirname, "dist-webpack"),
      filename: "example1.js"
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel"
      }
    ]
  }
};
