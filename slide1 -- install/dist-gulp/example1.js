"use strict";

var numbers = [1, 2, 3].map(function (x) {
  return x * x;
});
console.log("Numbers: ", numbers);