Drie voorbeelden om example1.js om te zetten van es2015 naar es5 met behulp van Babel
- Babel command line interface
- Gulp
- Webpack

example1.js

let numbers = [1,2,3].map(x => x * x);
console.log("Numbers: ", numbers);

------------------V------------------

"use strict";

var numbers = [1, 2, 3].map(function (x) {
  return x * x;
});
console.log("Numbers: ", numbers);

-------------------------------------

npm install babel-cli babel-preset-es2015 --save-dev
./node_modules/.bin/babel --presets es2015 example1.js

>> http://babeljs.io/docs/plugins/

-------------------------------------

.babelrc
{
  "presets": ["es2015"]
}

./node_modules/.bin/babel example1.js

>> http://babeljs.io/docs/usage/babelrc/

-------------------------------------

package.json

npm run babel

-------------------------------------

npm install --save-dev gulp
npm install --save-dev gulp-babel

gulpfile.js

gulp

-------------------------------------

npm install webpack --save-dev
npm install babel-loader babel-core babel-preset-es2015 --save-dev

webpack.config.js

package.json

npm run webpack

-------------------------------------

>> https://github.com/babel/babel-sublime

npm install babel-eslint

>> https://github.com/babel/babel-eslint

