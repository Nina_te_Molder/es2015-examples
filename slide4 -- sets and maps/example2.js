// Map

// Gebruik een Map ipv een object als de keys onbekend zijn tijdens runtime
// Gebruik een Map ipv een object als de types hetzelfde zijn

let article1 = {
  title: "Artikel 1",
  date: new Date()
};

let article2 = {
  title: "Artikel 2",
  date: new Date()
};

let articlesObj = {};
articlesObj[article1] = 23;
articlesObj[article2] = 452;

console.log("2nd article from object", articlesObj[article1]);
console.log(Object.keys(articlesObj));

let articlesMap = new Map();
articlesMap.set(article1, 23);
articlesMap.set(article2, 452);

console.log("2nd article from map", articlesMap.get(article1));
