// For...of

// Gebruik for...of ipv for...in om door Arrays te loopen
// De code is simpel en leesbaar in vergelijking met de ES5 variant

let newsPapers = ['Volkskrant', 'Trouw', 'Parool'];

for (let newspaper in newsPapers) {
  console.log(newsPapers[newspaper]);
}

console.log('-------------------------------------');

for (let newspaper of newsPapers) {
  console.log(newspaper);
}

// For...of kan niet worden gebruikt om door de properties van een object te loopen
