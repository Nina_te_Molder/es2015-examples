// Set

// De waardes in een Array hoeven niet uniek te zijn
// De waardes in een Set moeten uniek zijn
// Als je een waarde probeert toe te voegen die al bestaat, wordt deze genegeerd

let tags = [];
tags.push("Amsterdam");
tags.push("Amsterdam");
tags.push("Stadsgids");

console.log("Aantal tags in de Array", tags.length);

console.log("------------------------------------")

let tagSet = new Set();
tagSet.add("Amsterdam");
tagSet.add("Amsterdam");
tagSet.add("Stadsgids");

for (let tag of tagSet) {
  console.log("tag", tag);
}
console.log("Heeft een tag Amsterdam?", tagSet.has("Amsterdam"));
console.log("Heeft een tag Binnenland?", tagSet.has("Binnenland"));

console.log("Aantal tags in de Set", tagSet.size);
