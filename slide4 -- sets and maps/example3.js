// WeakMaps

// Bij WeakMaps kunnen alleen objecten (geen strings, nummers en booleans) gebruikt worden als key
// WeakMaps zijn niet iterable, er kan geen for...of worden gebruikt
// WeakMaps zijn efficienter in het gebruik van geheugen,
// individuele elementen kunnen met garbage collection worden opgeruimd

let user1 = {};
let user2 = {};

let map = new WeakMap();
map.set(user1, "logged-in");
map.set(user2, "logged-out");

console.log(map.get(user1));
console.log(map.get(user2));
