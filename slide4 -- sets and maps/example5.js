// WeakSet

// WeakSet is een Set waar alleen objecten aan mogen worden toegevoegd.
// Je kan niet door een WeakSet loopen met for...of
// Ook kan je geen waardes lezen van een Weakset

let article1 = { title: "Artikel 1" };
let article2 = { title: "Artikel 2" };
let article3 = { title: "Artikel 3" };
let article4 = { title: "Artikel 4" };

let articles = [];
articles.push(article1);
articles.push(article2);
articles.push(article3);
articles.push(article4);

// WeakSets zijn ideaal om groupen van objecten te creëren die een overeenkomstige eigenschap hebben
// zonder een object te mutaten

// Dit zal het object muteren!
// article2.isRead = true;
// article3.isRead = true;

// console.log("Read articles:");
// for (let article in articles) {
//   if (article.isRead) {
//     console.log(article.title);
//   }
// }

// In plaats van het object te muteren voegen we het toe aan een WeakSet
// Immutable objecten zorgen voor simpelere code met minder onverwachte side effects
// >> http://jlongster.com/Using-Immutable-Data-Structures-in-JavaScript

let readArticles = new WeakSet();
readArticles.add(article2);
readArticles.add(article3);

console.log("Read articles:");
for (let article of articles) {
  if (readArticles.has(article)) {
    console.log(article.title);
  }
}
